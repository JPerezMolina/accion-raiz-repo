$(function () {
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
  $(".carousel").carousel({
    interval: 4000,
  });

  $("#virtualAssistant").on("show.bs.modal", function (e) {
    console.log("El modal Asistente Virtual se está mostrando");
    $("#chat").removeClass("btn-secondary");
    $("#chat").addClass("btn-primary");
    $("#chat").prop("disabled", true);
  });
  $("#virtualAssistant").on("shown.bs.modal", function (e) {
    console.log("El modal Asistente Virtual se mostró");
  });

  $("#virtualAssistant").on("hide.bs.modal", function (e) {
    console.log("El modal Asistente Virtual se está ocultando");
    $("#chat").removeClass("btn-primary");
    $("#chat").addClass("btn-secondary");
    $("#chat").prop("disabled", false);
  });
  $("#virtualAssistant").on("hidden.bs.modal", function (e) {
    console.log("El modal Asistente Virtual se ocultó");
  });
});
