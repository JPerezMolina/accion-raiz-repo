Acción Raiz
=============



Descripción
-------------

Este proyecto está orientado a facilitar a las personas la compra y venta de inmuebles.




Instrucciones
---------------

 • Pre-requisitos
  
   - Instalar VCS (sistema de control de versiones)

   - Crear cuenta en Bitbucket

   - Editor de preferencia

 • Instalación

   - Ejecutar Git Bash

   - Acceder mediante comandos al directorio o carpeta donde descargaremos el repositorio

   - Ejecutar el siguiente comando:  git clone https://JuanCPM@bitbucket.org/JuanCPM/b-accion_raiz-repo.git



Control de versiones
----------------------

Este proyecto se encuentra en su versión 1.0.0  Beta


Autor
-------

Juan Carlos Pérez Molina



Licencia
----------

Copyright (c) 2020 Juan Carlos Pérez Molina 



Agradecimientos
-----------------

- Alcaldía de Medellín en convenio con Coursera

- Ezequiel Lamónica